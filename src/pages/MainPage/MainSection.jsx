import { addProductToCart } from "../../stores/actions";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "../../stores/actions";
import Modal from "../../components/Modal/Modal";
import Card from "../../components/Card/Card";
import { useEffect, useState } from "react";
import "./MainSection.scss";
import ChangeStyleButton from "./ChangeStyleButton";
import { createContext } from "react";
export const styleContext = createContext();
function MainSection() {
  const [style, setChangeStyle] = useState(true);

  const dispatch = useDispatch();
  const products = useSelector(
    (state) => state.getAllProductsReducer.allProducts
  );
  const btnMessage = "Add to cart";

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  const submitToAddToCart = () => {
    dispatch(addProductToCart());
  };
  return (
    <>
      <Modal
        header="Add this item to the cart?"
        textContent="Are you sure you want to add this item to the card? "
        submitBtnFunc={submitToAddToCart}
      />
      <div className="toogle_style">
        <ChangeStyleButton setChangeStyle={setChangeStyle} />
      </div>

      <div className="main-section">
        <ul className="main-section__list">
          {products.map((product) => (
            <styleContext.Provider value={style}>
              <Card
                // style={style}
                id={product.id}
                key={product.id}
                name={product.name}
                price={product.price}
                image={product.image}
                vendorCode={product.vendorCode}
                btnMessage={btnMessage}
              />
            </styleContext.Provider>
          ))}
        </ul>
      </div>
    </>
  );
}

export default MainSection;

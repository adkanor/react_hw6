import getAllProductsReducer from "./reducerAllProducts";
import { FETCH_PRODUCTS } from "./actions";
import { cleanup } from "@testing-library/react";
afterEach(() => {
  cleanup();
});

describe("getAllProductsReducer", () => {
  it("handles FETCH_PRODUCTS action correctly", () => {
    const initialState = {
      allProducts: [],
    };

    const products = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    const action = { type: FETCH_PRODUCTS, payload: products };

    const nextState = getAllProductsReducer(initialState, action);
    expect(nextState).toEqual({
      allProducts: products,
    });
  });

  it("handles unknown action type", () => {
    const initialState = {
      allProducts: [],
    };

    const action = { type: "UNKNOWN_ACTION" };

    const nextState = getAllProductsReducer(initialState, action);

    expect(nextState).toEqual(initialState);
  });

  it("uses initial state when current state is undefined", () => {
    const products = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    const action = { type: FETCH_PRODUCTS, payload: products };

    const nextState = getAllProductsReducer(undefined, action);

    expect(nextState).toEqual({
      allProducts: products,
    });
  });
});

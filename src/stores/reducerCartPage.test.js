import cartReducer from "./reducerCartPage";
import {
  TRY_TO_ADD_TO_CART,
  TRY_TO_REMOVE_FROM_CART,
  ADD_TO_CART,
  REMOVE_FROM_CART,
  RESET_CART,
  CHECK_OUT_BTN_ABLED,
  CHECK_OUT_BTN_DISABLED,
} from "./actions";
import { cleanup } from "@testing-library/react";
afterEach(() => {
  cleanup();
});

describe("cartReducer", () => {
  it("handles TRY_TO_ADD_TO_CART action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [],
      checkOutBtn: false,
    };
    const action = {
      type: TRY_TO_ADD_TO_CART,
      payload: { id: 1, name: "Product 1" },
    };
    const nextState = cartReducer(initialState, action);

    expect(nextState.cardToAdd).toEqual({ id: 1, name: "Product 1" });
  });

  it("handles ADD_TO_CART action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [],
      checkOutBtn: false,
    };
    initialState.cardToAdd = { id: 1, name: "Product 1" };
    const action = { type: ADD_TO_CART };
    const nextState = cartReducer(initialState, action);

    expect(nextState.allProducts).toEqual([{ id: 1, name: "Product 1" }]);
  });

  it("handles unknown action type", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [],
      checkOutBtn: false,
    };
    const action = { type: "UNKNOWN_ACTION" };
    const nextState = cartReducer(initialState, action);

    expect(nextState).toEqual(initialState);
  });
  it("handles TRY_TO_REMOVE_FROM_CART action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [],
      checkOutBtn: false,
    };
    const action = {
      type: TRY_TO_REMOVE_FROM_CART,
      payload: { id: 1, name: "Product 1" },
    };
    const nextState = cartReducer(initialState, action);

    expect(nextState.cardToRemove).toEqual({ id: 1, name: "Product 1" });
  });
  it("handles REMOVE_FROM_CART action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: { id: 1, name: "Product 1" },
      allProducts: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
      checkOutBtn: false,
    };
    const action = { type: REMOVE_FROM_CART };
    const nextState = cartReducer(initialState, action);

    expect(nextState.allProducts).toEqual([{ id: 2, name: "Product 2" }]);
  });
  it("handles RESET_CART action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
      checkOutBtn: true,
    };
    const action = { type: RESET_CART };
    const nextState = cartReducer(initialState, action);

    expect(nextState.allProducts).toEqual([]);
  });
  it("handles CHECK_OUT_BTN_ABLED action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [],
      checkOutBtn: false,
    };
    const action = { type: CHECK_OUT_BTN_ABLED };
    const nextState = cartReducer(initialState, action);

    expect(nextState.checkOutBtn).toBe(true);
  });

  it("handles CHECK_OUT_BTN_DISABLED action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: {},
      allProducts: [],
      checkOutBtn: true,
    };
    const action = { type: CHECK_OUT_BTN_DISABLED };
    const nextState = cartReducer(initialState, action);

    expect(nextState.checkOutBtn).toBe(false);
  });
  // Simulate adding a product to the cart
  it("updates localStorage on ADD_TO_CART action", () => {
    const initialState = {
      cardToAdd: { id: 3, name: "Product 3" },
      cardToRemove: {},
      allProducts: [{ id: 1, name: "Product 1" }],
      checkOutBtn: false,
    };
    const action = { type: ADD_TO_CART };
    cartReducer(initialState, action);

    const storedProducts = JSON.parse(localStorage.getItem("cartProducts"));
    expect(storedProducts).toEqual([
      { id: 1, name: "Product 1" },
      { id: 3, name: "Product 3" },
    ]);
  });

  // Simulate removing a product from the cart
  it("updates localStorage on REMOVE_FROM_CART action", () => {
    const initialState = {
      cardToAdd: {},
      cardToRemove: { id: 1, name: "Product 1" },
      allProducts: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
      checkOutBtn: false,
    };
    const action = { type: REMOVE_FROM_CART };
    cartReducer(initialState, action);

    const storedProducts = JSON.parse(localStorage.getItem("cartProducts"));
    expect(storedProducts).toEqual([{ id: 2, name: "Product 2" }]);
  });
});

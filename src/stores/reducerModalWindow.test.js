import modalReducer from "./reducerModalWindow";
import { TOOGLE_MODAL } from "./actions";
import { cleanup } from "@testing-library/react";
afterEach(() => {
  cleanup();
});

describe("modalReducer", () => {
  it("handles unknown action type", () => {
    const initialState = { isOpened: false };
    const action = { type: "UNKNOWN_ACTION" };
    const nextState = modalReducer(initialState, action);

    expect(nextState).toEqual(initialState);
  });
  it("toggles isOpened state on TOGGLE_MODAL action", () => {
    const initialState = { isOpened: false };
    const toggleAction = { type: TOOGLE_MODAL };

    const nextState1 = modalReducer(initialState, toggleAction);
    console.log(nextState1.isOpened);
    expect(nextState1.isOpened).toBe(true);

    const nextState2 = modalReducer(nextState1, toggleAction);
    expect(nextState2.isOpened).toBe(false);
  });
});

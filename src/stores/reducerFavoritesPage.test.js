import favoritesReducer from "./reducerFavoritesPage";
import { ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES } from "./actions";
import { cleanup } from "@testing-library/react";
afterEach(() => {
  cleanup();
});

describe("favoritesReducer", () => {
  it("handles ADD_TO_FAVORITES action", () => {
    const initialState = { favorites: [] };
    const action = {
      type: ADD_TO_FAVORITES,
      payload: { id: 1, name: "Product 1" },
    };
    const nextState = favoritesReducer(initialState, action);

    expect(nextState.favorites).toEqual([{ id: 1, name: "Product 1" }]);
  });

  it("handles REMOVE_FROM_FAVORITES action", () => {
    const initialState = {
      favorites: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
    };
    const action = { type: REMOVE_FROM_FAVORITES, payload: 1 };
    const nextState = favoritesReducer(initialState, action);

    expect(nextState.favorites).toEqual([{ id: 2, name: "Product 2" }]);
  });
  it("updates localStorage on ADD_TO_FAVORITES action", () => {
    const initialState = { favorites: [] };
    const action = {
      type: ADD_TO_FAVORITES,
      payload: { id: 1, name: "Product 1" },
    };
    favoritesReducer(initialState, action);

    const storedFavorites = JSON.parse(localStorage.getItem("favoriteItems"));
    expect(storedFavorites).toEqual([{ id: 1, name: "Product 1" }]);
  });

  it("updates localStorage on REMOVE_FROM_FAVORITES action", () => {
    const initialState = {
      favorites: [
        { id: 1, name: "Product 1" },
        { id: 2, name: "Product 2" },
      ],
    };
    const action = { type: REMOVE_FROM_FAVORITES, payload: 1 };
    favoritesReducer(initialState, action);

    const storedFavorites = JSON.parse(localStorage.getItem("favoriteItems"));
    expect(storedFavorites).toEqual([{ id: 2, name: "Product 2" }]);
  });
  it("handles unknown action type", () => {
    const initialState = { favorites: [] };
    const action = { type: "UNKNOWN_ACTION" };
    const nextState = favoritesReducer(initialState, action);

    expect(nextState).toEqual(initialState);
  });
});

import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import AddToCart from "./AddToCart";
import { TOOGLE_MODAL } from "../../stores/actions"; // Import your TOGGLE_MODAL action type

const mockStore = configureStore([]);

describe("AddToCart Component", () => {
  it("triggers tryToCart function and dispatches TOGGLE_MODAL action on button click", () => {
    const tryToCartMock = jest.fn();
    const initialState = {
      modalReducer: {
        isOpened: false,
      },
    };
    const store = mockStore(initialState);

    const { getByText } = render(
      <Provider store={store}>
        <AddToCart
          backgroundColor="rgb(116, 34, 50)"
          text="Add to Cart"
          tryToCart={tryToCartMock}
        />
      </Provider>
    );

    const button = getByText("Add to Cart");

    // Click the button
    fireEvent.click(button);

    // Check if tryToCart function is called
    expect(tryToCartMock).toHaveBeenCalled();

    // Check if TOGGLE_MODAL action is dispatched
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toContainEqual({ type: TOOGLE_MODAL });
  });

  it("renders button with correct background color and text", () => {
    const tryToCartMock = jest.fn();
    const initialState = {
      modalReducer: {
        isOpened: false,
      },
    };
    const store = mockStore(initialState);

    const { getByText } = render(
      <Provider store={store}>
        <AddToCart
          backgroundColor="rgb(116, 34, 50)"
          text="Custom Text"
          tryToCart={tryToCartMock}
        />
      </Provider>
    );

    const button = getByText("Custom Text");

    expect(button).toHaveStyle("background-color: rgb(116, 34, 50)");

    expect(button).toHaveTextContent("Custom Text");
  });
});

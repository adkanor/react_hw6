import React from "react";
import { Provider } from "react-redux";
import store from "../../stores/store";
import CartItem from "./CartItem";
import renderer from "react-test-renderer";

test("renders CartItem component correctly", () => {
  const props = {
    id: 1,
    image: "path-to-image.jpg",
    name: "Product Name",
    price: "100.00",
    vendorCode: 123456,
    btnMessage: "Add to Cart",
  };

  const component = renderer.create(
    <Provider store={store}>
      <CartItem {...props} />
    </Provider>
  );
  const tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});

import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Modal from "./Modal";

const mockStore = configureStore([]);
const initialState = {
  modalReducer: {
    isOpened: false,
  },
};
let store = mockStore(initialState);

describe("Modal Component", () => {
  it("renders correctly with closed modal", () => {
    const { container } = render(
      <Provider store={store}>
        <Modal
          header="Test Header"
          textContent="Test Content"
          submitBtnFunc={() => {}}
        />
      </Provider>
    );

    const modal = container.querySelector(".modal");
    expect(modal).toHaveClass("modal");
  });
  it("renders modal with correct header and text content", () => {
    const { getByText } = render(
      <Provider store={store}>
        <Modal
          header="Test Header"
          textContent="Test Content"
          submitBtnFunc={() => {}}
        />
      </Provider>
    );

    const headerElement = getByText("Test Header");
    const textContentElement = getByText("Test Content");

    expect(headerElement).toBeInTheDocument();
    expect(textContentElement).toBeInTheDocument();
  });
  it("calls submitBtnFunc on OK button click", () => {
    const mockSubmitFunc = jest.fn(); // Create a mock function
    const { getByText } = render(
      <Provider store={store}>
        <Modal
          header="Test Header"
          textContent="Test Content"
          submitBtnFunc={mockSubmitFunc}
        />
      </Provider>
    );

    const okButton = getByText("OK");
    fireEvent.click(okButton);

    expect(mockSubmitFunc).toHaveBeenCalledTimes(1);
  });
});

import React from "react";
import renderer from "react-test-renderer";
import FullScreenWrapper from "./FullScreenWrapper";

test("renders FullScreenWrapper component correctly", () => {
  const component = renderer.create(
    <FullScreenWrapper>
      <p>Content goes here</p>
    </FullScreenWrapper>
  );
  const tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});

import React from "react";
import renderer from "react-test-renderer";
import FavoriteIcon from "./FavoriteIcon";

test("renders FavoriteIcon component correctly", () => {
  const props = {
    thisCard: { id: 1 },
    favoritesChecker: () => false,
    addToFavs: () => {},
    removeFromFavs: () => {},
  };

  const component = renderer.create(<FavoriteIcon {...props} />);
  const tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});
